#pragma once
#include <string>
using namespace std;

class stringTester
{
public:
	stringTester();
	bool isSlip(string);
	bool isSlap(string);
	bool isSlop(string);
};

