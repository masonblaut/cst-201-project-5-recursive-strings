// Mason Blaut
//	This is my own work.

#include <iostream>
#include <fstream>
#include "stringTester.h"
using namespace std;

int main()
{

	//Test for Slip
	stringTester test;
	string stringSlip = "DFFFFFFFFDFFFFFFG";
	
	bool result = test.isSlip(stringSlip);

	if (result == false)
	{
		cout << "\n" << stringSlip << " is not a slip" << "\n";
	}
	else if (result == true)
	{
		cout << "\n" << stringSlip << " is a slip" << "\n";
	}


	//Test for Slap
	cout << "\n";
	string stringSlap = "ADFFFFFFFFDFFFFFFGC";
	bool resultSlap = test.isSlap(stringSlap);

	if (resultSlap == false)
	{
		cout << "\n" << stringSlap << " is not a slap" << "\n";
	}
	else if (resultSlap == true)
	{
		cout << "\n" << stringSlap << " is a slap" << "\n";
	}

	//Test for Slop
	cout << "\n";
	string stringSlop = "ABAEFGCCDFEFFFFFG";
	bool resultSlop = test.isSlop(stringSlop);

	if (resultSlop == false)
	{
		cout << "\n" << stringSlop << " is not a slop" << "\n";
	}
	else if (resultSlop == true)
	{
		cout << "\n" << stringSlop << " is a slop" << "\n";
	}

	
	//////////
	cout << "\n";


	ifstream myfile;
	myfile.open("slipslapslop.txt");
	string str;
	getline(myfile, str);
	int strNum = stoi(str);
	cout << strNum << "\n";
	string* testStrings = new string[strNum];
	cout << "SLOPS OUTPUT\n";
	for (int i = 0; i < strNum; i++)
	{
		getline(myfile, testStrings[i]);
		bool isSlop = test.isSlop(testStrings[i]);
		if (isSlop) 
		{
			cout << "YES\n";
		}
		else
		{
			cout << "NO\n";
		}
		
	}
	cout << "END OF OUTPUT\n";
	

    cout << "\nEND\n";
}